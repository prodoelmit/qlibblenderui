#include "abstractpanebar.h"
#include "qdebug.h"
AbstractPaneBar::AbstractPaneBar(AbstractPaneWidget *parent):
	QStackedWidget(parent)
{
	m_paneLayout = qobject_cast<AbstractPaneLayout*>(parent->layout());
	m_infoLabel = new QLabel("Abstract");
	m_toolbar = new QToolBar(this);
	m_toolbar->addWidget(m_paneLayout->roleSwitcher());
	addWidget(m_toolbar);
	addWidget(m_infoLabel);
	setCurrentWidget(m_toolbar);
	QSizePolicy toolBarPolicy;
	toolBarPolicy.setVerticalStretch(0);
	toolBarPolicy.setVerticalPolicy(QSizePolicy::Minimum);
	setSizePolicy(toolBarPolicy);
}

AbstractPaneBar::~AbstractPaneBar()
{

}

void AbstractPaneBar::startShowingInfo()
{
	setCurrentWidget(m_infoLabel);
}

void AbstractPaneBar::setInfo(QString info)
{
	qDebug() << "AbstractPaneBar::setInfo(" + info + ")";
	m_infoLabel->setText(info);
}

void AbstractPaneBar::stopShowingInfo()
{
    setCurrentWidget(m_toolbar);
}

QLabel *AbstractPaneBar::label()
{return m_infoLabel;

}

