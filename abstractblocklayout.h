#ifndef ABSTRACTBLOCKLAYOUT_H
#define ABSTRACTBLOCKLAYOUT_H
#include <QVBoxLayout>
#include <QMenuBar>
#include <QLayout>
#include <QSplitter>
#include <QLabel>
#include "abstractpane.h"
#include "windowblock.h"
class WindowBlock;
class AbstractBlockLayout : public QVBoxLayout
{
public:
    AbstractBlockLayout(WindowBlock *parentBlock);
	~AbstractBlockLayout();
	QMenuBar* menuBar();
	QLabel *labelBar();

    WindowBlock* parentBlock();
	QSplitter* workZone();
	void showLabelInsteadMenu();
private:
    WindowBlock* m_parentBlock;
    QMenuBar* m_menuBar;
	QLabel* m_labelBar;
	QSplitter* m_workZone;
};

#endif // ABSTRACTBLOCKLAYOUT_H
