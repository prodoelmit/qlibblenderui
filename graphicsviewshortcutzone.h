#ifndef GRAPHICSVIEWSHORTCUTZONE_H
#define GRAPHICSVIEWSHORTCUTZONE_H
#include "shortcutzone.h"
#include <QGraphicsView>
class GraphicsViewShortcutZone: public QGraphicsView
{
public:
	GraphicsViewShortcutZone();
	void addShortcut(QShortcut* shortcut);
	void removeShortcut(QShortcut* shortcut);
	void clearShortcuts();
	void enterEvent(QEvent *);
	void leaveEvent(QEvent *);
private:
	QList<QShortcut* > m_shortcuts;
};

#endif // GRAPHICSVIEWSHORTCUTZONE_H
