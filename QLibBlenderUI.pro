#-------------------------------------------------
#
# Project created by QtCreator 2014-12-18T15:09:36
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QLibBlenderUI

DEFINES += QLIBBLENDERUI_LIBRARY

TEMPLATE = lib

SOURCES +=  \
#abstractblocklayout.cpp \
abstractpanebar.cpp \
abstractpane.cpp \
abstractpanelayout.cpp \
abstractpanewidget.cpp \
#graphicsviewshortcutzone.cpp \
#gui.cpp \
operationperformer.cpp \
paneroleswitcher.cpp \
shortcutarea.cpp \
#shortcutzone.cpp \
#windowblock.cpp

HEADERS += \
#abstractblocklayout.h \
abstractpanebar.h \
abstractpane.h \
abstractpanelayout.h \
abstractpanewidget.h \
#graphicsviewshortcutzone.h \
#gui.h \
operationperformer.h \
paneroleswitcher.h \
qlibblenderui_global.h \
shortcutarea.h \
#shortcutzone.h




