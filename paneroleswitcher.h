#ifndef PANEROLESWITCHER_H
#define PANEROLESWITCHER_H
#include <QToolButton>
#include <QMenu>
#include <QAction>
#include "abstractpane.h"
#include "gui.h"
#include "qlibblenderui_global.h"
class AbstractPane;
class QLIBBLENDERUISHARED_EXPORT PaneRoleSwitcher: public QToolButton
{
	Q_OBJECT
public:
	PaneRoleSwitcher(AbstractPane *pane);
	~PaneRoleSwitcher();
	void setPane(AbstractPane* pane);
    void switchRole(int role);
public slots:

private:
    int m_currentRole;
	AbstractPane* m_pane;

};

#endif // PANEROLESWITCHER_H
