#include "abstractpanelayout.h"

AbstractPaneLayout::AbstractPaneLayout(AbstractPaneWidget* parent):
	QGridLayout(parent)
{
//	m_bar = new AbstractPaneBar(parent);
}

AbstractPaneLayout::~AbstractPaneLayout()
{

}

PaneRoleSwitcher *AbstractPaneLayout::roleSwitcher()
{
	return pane()->roleSwitcher();
}

AbstractPaneBar *AbstractPaneLayout::bar()
{
	return m_bar;
}

AbstractPane *AbstractPaneLayout::pane()
{
	return parentWidget()->parentPane();
}

AbstractPaneWidget* AbstractPaneLayout::parentWidget() const {
	return qobject_cast<AbstractPaneWidget*>(QLayout::parentWidget());
}

