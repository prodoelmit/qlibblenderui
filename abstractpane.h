#ifndef ABSTRACTPANE_H
#define ABSTRACTPANE_H
#include <QSplitter>
#include "paneroleswitcher.h"
#include "abstractpanelayout.h"
#include "gui.h"
#include <QSet>
#include "abstractpanewidget.h"
#include "qlibblenderui_global.h"
class AbstractPaneLayout;
class QLIBBLENDERUISHARED_EXPORT AbstractPane: public QSplitter
{
	Q_OBJECT
public:
	enum PaneRole {XZRole};
    AbstractPane(QWidget *parent = 0, int role = Gui::XZPaneRole);
    AbstractPane(AbstractPaneLayout* paneLayout, int role, QWidget *parent = 0);
	~AbstractPane();
	AbstractPaneLayout* widgetLayout() const;
	QList<AbstractPane*> children() const;
    void switchRole(int role);
    int role();
	PaneRoleSwitcher* roleSwitcher();
    AbstractPane* clone();
	bool eventFilter(QObject *o, QEvent *e);
	void removePane(int i);
	bool deepest();
    virtual AbstractPane* createPane(AbstractPaneLayout *layout, int role);
    AbstractPaneWidget *cWidget() const;

protected slots:

    virtual void splitHorizontally();
    virtual void splitVertically();
	void splitterMovedEvent();
protected:
	bool m_is_deepest;
	QList<AbstractPane*> m_children;
	AbstractPaneLayout* m_widgetLayout;
	AbstractPaneWidget* m_cWidget;
    int m_role;
	PaneRoleSwitcher* m_roleSwitcher;
};

#endif // ABSTRACTPANE_H
