#include "operationperformer.h"
#include "qdebug.h"


OperationPerformer::OperationPerformer(int inputSize, AbstractPaneBar *bar):
    m_bar(bar)
{
	m_inputSize = inputSize;
	for (int i = 0; i < m_inputSize; i++) {
		m_inputList << QString("NONE");
		m_valuesList << 0;
	}
    m_currentInputIterator = m_inputList.begin();
	grabKeyboard();
    grabMouse();
	m_listeningMouse = true;
    m_bar->setInfo(getBarText());
    m_bar->startShowingInfo();
    m_startMousePos = mapFromGlobal(QCursor::pos());

}



OperationPerformer::~OperationPerformer()
{
	releaseKeyboard();
    m_bar->stopShowingInfo();
    disconnect(this,0,0,0);
}

void OperationPerformer::setWorkingWidget(QWidget *widget)
{
	m_workingWidget = widget;
}

QWidget *OperationPerformer::workingWidget()
{
	return m_workingWidget;
}

void OperationPerformer::escape()
{
    qDebug() << "opPerformer::escape()";
	emit escaped();

}

void OperationPerformer::enter()
{
    qDebug() << "opPerformer::enter()";
	emit entered();
}

void OperationPerformer::input(char c)
{
	QString& currentInput = *m_currentInputIterator;
	if (((int)c <= (int)'9' && (int)c >= '0')
		||
        c == '.'
            ||
            c == '-') {
		if (currentInput == "NONE") {
			currentInput.clear();
		}
		currentInput.append(c);
		m_listeningMouse = false;
        releaseMouse();
		setValuesFromInput();
        updateBar();
    }
}

void OperationPerformer::backspace()
{
    QString& currentInput = *m_currentInputIterator;
    currentInput = currentInput.remove(currentInput.size()-1,1);
    setValuesFromInput();
    updateBar();

}

void OperationPerformer::setValuesFromInput()
{
	for (int i = 0; i < m_inputSize; i++) {
		m_valuesList[i] = m_inputList[i].toDouble();
	}
}

void OperationPerformer::setValues(QList<qreal> values)
{
	for (int i = 0; i < m_inputSize; i++) {
		m_valuesList[i] = values[i];
	}
}

void OperationPerformer::updateBar()
{
    m_bar->setInfo(getBarText());

}

AbstractPaneBar *OperationPerformer::bar()
{
    return m_bar;
}



void OperationPerformer::keyPressEvent(QKeyEvent *e)
{
    qDebug() << "OperationPerformer::keyPressEvent" << e;
	if (e->key() == Qt::Key_Escape) {
        escape();
    } else if (e->key() == Qt::Key_Enter || e->key() == Qt::Key_Return){
		enter();
	} else {
        if (m_listeningMouse) {
            for (int i = 0; i < m_inputList.size(); i++) {
                m_inputList[i] = "NONE";
            }
			m_currentInputIterator = m_inputList.begin();
		}
		if (e->key() == Qt::Key_Tab) {
			m_currentInputIterator++;
			if (m_currentInputIterator == m_inputList.end()) {
				m_currentInputIterator = m_inputList.begin();
			}
            updateBar();
        } else if (e->key() == Qt::Key_Backspace) {
            backspace();
        } else if (e->key() == Qt::Key_0) {
			input('0');
		} else if (e->key() == Qt::Key_1) {
			input('1');
		} else if (e->key() == Qt::Key_2) {
			input('2');
		} else if (e->key() == Qt::Key_3) {
			input('3');
		} else if (e->key() == Qt::Key_4) {
			input('4');
		} else if (e->key() == Qt::Key_5) {
			input('5');
		} else if (e->key() == Qt::Key_6) {
			input('6');
		} else if (e->key() == Qt::Key_7) {
			input('7');
		} else if (e->key() == Qt::Key_8) {
			input('8');
		} else if (e->key() == Qt::Key_9) {
			input('9');
		} else if (e->key() == Qt::Key_Period) {
			input('.');
        } else if (e->key() == Qt::Key_Minus) {
            input('-');
        }

	}


}

void OperationPerformer::mousePressEvent(QMouseEvent *e)
{
    m_startMousePos = e->pos();
}

bool OperationPerformer::isListeningMouse() const
{
    return m_listeningMouse;
}

void OperationPerformer::mouseMoveEvent(QMouseEvent *e)
{
    if (m_listeningMouse) {
        m_curMousePos = e->pos();
    }

}

QList<qreal> OperationPerformer::valuesList() const
{
    return m_valuesList;
}

void OperationPerformer::updateInputListOnValues()
{
    m_inputList = QList<QString>();
    foreach (qreal value, m_valuesList) {
        QString s = QString::number(value);
        m_inputList << s;
    }
    updateBar();

}

QString OperationPerformer::getBarText()
{
    QString s;
    bool first = true;
    for (QStringList::iterator it = m_inputList.begin(); it != m_inputList.end(); ++it) {
        if (!first) s+= "| ";
        if (it == m_currentInputIterator) {
            s += "~";
            s += *it;
            s += "~";
        } else {
            s += *it;
        }
        first = false;
    }
    return s;
}
