#include "graphicsviewshortcutzone.h"

GraphicsViewShortcutZone::GraphicsViewShortcutZone()
{
}
void GraphicsViewShortcutZone::addShortcut(QShortcut *shortcut)
{
	m_shortcuts << shortcut;
}

void GraphicsViewShortcutZone::removeShortcut(QShortcut *shortcut)
{
	m_shortcuts.removeAll(shortcut);
}

void GraphicsViewShortcutZone::clearShortcuts()
{
	qDeleteAll(m_shortcuts);
	m_shortcuts.clear();
}

void GraphicsViewShortcutZone::enterEvent(QEvent *) {
	for (int i = 0; i < m_shortcuts.size(); i++) {
		m_shortcuts[i]->setEnabled(true);
	}
}

void GraphicsViewShortcutZone::leaveEvent(QEvent *) {
	for (int i = 0; i < m_shortcuts.size(); i++) {
		m_shortcuts[i]->setEnabled(false);
	}
}
