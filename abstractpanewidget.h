#ifndef ABSTRACTPANEWIDGET_H
#define ABSTRACTPANEWIDGET_H
#include "abstractpane.h"
#include "abstractpanelayout.h"
#include <QWidget>
#include "abstractpanebar.h"
#include <QMouseEvent>
#include "qlibblenderui_global.h"
class AbstractPane;
class AbstractPaneBar;
class AbstractPaneLayout;
class QLIBBLENDERUISHARED_EXPORT AbstractPaneWidget : public QWidget
{
	Q_OBJECT
public:
	explicit AbstractPaneWidget(AbstractPane *parent = 0, AbstractPaneLayout *layout = 0);
	AbstractPaneBar* bar();
	AbstractPane* parentWidget() const;
	AbstractPane* parentPane() const;
	AbstractPaneLayout* layout() const;

    void updateConnections();
	void mousePressEvent(QMouseEvent *e);
signals:

public slots:

private:
	QMenu* m_splitMenu;
    QSet<QAction*> m_connectedActions;
    QAction* m_splitHorAction;
    QAction* m_splitVerAction;
};

#endif // ABSTRACTPANEWIDGET_H
