#ifndef OPERATIONPERFORMER_H
#define OPERATIONPERFORMER_H
#include <QMenuBar>
#include <QList>
#include <QWidget>
#include <QString>
#include <QLabel>
#include <QKeyEvent>
#include "qlibblenderui_global.h"
#include "abstractpanebar.h"
class QLIBBLENDERUISHARED_EXPORT OperationPerformer: public QWidget
{
	Q_OBJECT
public:
    OperationPerformer(int inputSize, AbstractPaneBar *bar);
	~OperationPerformer();
	void setWorkingWidget(QWidget* widget);
	QWidget* workingWidget();
    virtual void escape();
    virtual void enter();
	void input(char c);
    void backspace();
    virtual void setValuesFromInput();
	void setValues(QList<qreal> values);
	virtual void updateBar();
    AbstractPaneBar* bar();
    QList<qreal> valuesList() const;
    void updateInputListOnValues();
    QString getBarText();

    bool isListeningMouse() const;

public slots:
    virtual void mouseMoveEvent(QMouseEvent* e);
	virtual void keyPressEvent(QKeyEvent* e);
    virtual void mousePressEvent(QMouseEvent *e);
signals:
	void escaped();
	void entered();
protected:
    QList<qreal> m_valuesList;
    QPointF m_startMousePos;
    QPointF m_curMousePos;
private:
    AbstractPaneBar* m_bar;
	QWidget* m_workingWidget;
	QList<QString> m_inputList;
	QStringList::iterator m_currentInputIterator;
	QString m_menuString;
	bool m_listeningMouse;
	int m_inputSize;
};

#endif // OPERATIONPERFORMER_H
