#ifndef SHORTCUTAREA_H
#define SHORTCUTAREA_H

#include <QWidget>
#include <QShortcut>
#include <QSet>
#include "qlibblenderui_global.h"
class QLIBBLENDERUISHARED_EXPORT ShortcutArea : public QWidget
{
	Q_OBJECT
public:
	explicit ShortcutArea(QWidget *parent);
	void addShortcut(QShortcut* shortcut);
	void removeShortcut(QShortcut* shortcut);
	void muteAllShortcuts(bool evenEsc = false);
	void unmuteAllShortcuts();
signals:


public slots:
	void enterEvent(QEvent *);
	void leaveEvent(QEvent *);
protected:
private:
	QSet<QShortcut*> m_shortcuts;
	bool m_watching;
};

#endif // SHORTCUTAREA_H
