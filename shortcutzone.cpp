#include "shortcutzone.h"

ShortcutZone::ShortcutZone()
{
}

void ShortcutZone::addShortcut(QShortcut *shortcut)
{
	m_shortcuts << shortcut;
}

void ShortcutZone::removeShortcut(QShortcut *shortcut)
{
	m_shortcuts.removeAll(shortcut);
}

void ShortcutZone::clearShortcuts()
{
	qDeleteAll(m_shortcuts);
	m_shortcuts.clear();
}

void ShortcutZone::enterEvent(QEvent *) {
	for (int i = 0; i < m_shortcuts.size(); i++) {
		m_shortcuts[i]->setEnabled(true);
	}
}

void ShortcutZone::leaveEvent(QEvent *) {
	for (int i = 0; i < m_shortcuts.size(); i++) {
		m_shortcuts[i]->setEnabled(false);
	}
}
