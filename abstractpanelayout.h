#ifndef ABSTRACTPANELAYOUT_H
#define ABSTRACTPANELAYOUT_H
#include <QVBoxLayout>
#include "abstractpane.h"
#include "paneroleswitcher.h"
#include "abstractpanebar.h"
#include "abstractpanewidget.h"
#include "qlibblenderui_global.h"
class PaneRoleSwitcher;
class AbstractPane;
class AbstractPaneBar;
class AbstractPaneWidget;
class QLIBBLENDERUISHARED_EXPORT AbstractPaneLayout: public QGridLayout
{
	Q_OBJECT
public:
	AbstractPaneLayout(AbstractPaneWidget *parent = 0);
	~AbstractPaneLayout();
	PaneRoleSwitcher* roleSwitcher();
	AbstractPaneBar* bar();
	AbstractPane* pane();
	AbstractPaneWidget *parentWidget() const;
protected:
	AbstractPaneBar* m_bar;
};

#endif // ABSTRACTPANELAYOUT_H
