#include "abstractpane.h"
#include "qdebug.h"

AbstractPane::AbstractPane(QWidget* parent, int role):
	QSplitter(parent), m_is_deepest(true),
	m_role(role)
{
//    m_roleSwitcher = new PaneRoleSwitcher(this);
//    m_cWidget = new AbstractPaneWidget(this);
//    m_widgetLayout = new AbstractPaneLayout(m_cWidget);
//    m_cWidget->setLayout(m_widgetLayout);
//    if (m_is_deepest) {
//        addWidget(m_cWidget);
//    }
//    splitHorizontally();

//	switchRole(role);
//	switchRole(role);
//	switchRole(role);
//	QShortcut* splitHorizontallyShortcut = new QShortcut(QKeySequence("Ctrl+E,H"),this);
//	splitHorizontallyShortcut->setContext(Qt::WidgetShortcut);
//	connect(splitHorizontallyShortcut,SIGNAL(activated()),this,SLOT(splitHorizontally()));
//	qcmw->installEventFilter(this);
	connect(this,SIGNAL(splitterMoved(int,int)),this,SLOT(splitterMovedEvent()));
}

AbstractPane::AbstractPane(AbstractPaneLayout *paneLayout, int role, QWidget *parent):
	QSplitter(parent), m_is_deepest(true), m_role(role)
{
//	m_roleSwitcher = new PaneRoleSwitcher(this);
//	m_cWidget = new AbstractPaneWidget(this);
//	m_widgetLayout = paneLayout;
//	m_cWidget->setLayout(m_widgetLayout);
//	if (m_is_deepest) {
//		addWidget(m_cWidget);
//	}
}


bool AbstractPane::eventFilter(QObject *o, QEvent *e) {
	if (e->type() == QEvent::Shortcut) {
		QShortcutEvent* event = static_cast<QShortcutEvent*>(e);
		qDebug() << event->key();
		if (event->key() == QKeySequence("Ctrl+E,H") && hasFocus()) {
			qDebug() << "eventFilter::splitHorizontally()";
			splitHorizontally();
			return true;
		} else {
			return false;
		}
	} else {
		return QObject::eventFilter(o,e);
	}
}


AbstractPane::~AbstractPane()
{
}

AbstractPaneLayout *AbstractPane::widgetLayout() const
{
	return m_widgetLayout;
}


QList<AbstractPane *> AbstractPane::children() const
{
	return m_children;
}



PaneRoleSwitcher *AbstractPane::roleSwitcher()
{
	return m_roleSwitcher;
}

AbstractPane *AbstractPane::clone()
{
	AbstractPane* pane = new AbstractPane(parentWidget(),role());
	return pane;
}

void AbstractPane::splitHorizontally()
{
	m_is_deepest = false;
	setOrientation(Qt::Horizontal);
    AbstractPane* leftPane = createPane(m_widgetLayout, role());
	delete m_cWidget;
	AbstractPane* rightPane = leftPane->clone();
//	AbstractPane* hiddenLeftPane = leftPane->clone();
//	AbstractPane* hiddenRightPane = leftPane->clone();
//	addWidget(hiddenLeftPane);
	addWidget(leftPane);
	addWidget(rightPane);
//	addWidget(hiddenRightPane);
//	m_children.append(hiddenLeftPane);
	m_children.append(leftPane);
	m_children.append(rightPane);
//	m_children.append(hiddenRightPane);
}

void AbstractPane::splitVertically()
{
	m_is_deepest = false;
	qDebug() << "splitHorizontally()";
	setOrientation(Qt::Vertical);
	AbstractPane* topPane = new AbstractPane(m_widgetLayout,role());
	delete m_cWidget;
	AbstractPane* botPane = topPane->clone();
//	AbstractPane* hiddenLeftPane = leftPane->clone();
//	AbstractPane* hiddenRightPane = leftPane->clone();
//	addWidget(hiddenLeftPane);
	addWidget(topPane);
	addWidget(botPane);
//	addWidget(hiddenRightPane);
//	m_children.append(hiddenLeftPane);
	m_children.append(topPane);
	m_children.append(botPane);
}

void AbstractPane::splitterMovedEvent()
{
	QList<int> paneSizes = sizes();
	qDebug() << paneSizes;
	for (int i = 0; i < paneSizes.size(); i++) {
		if (paneSizes.at(i) == 0) {
			removePane(i);
		}
	}
}

AbstractPaneWidget *AbstractPane::cWidget() const
{
    return m_cWidget;
}



void AbstractPane::removePane(int i)
{
    delete widget(i);
    qDebug() << "After deleting: " << sizes();
	AbstractPane* survivor = static_cast<AbstractPane*>(widget(0));
	if (survivor->deepest()) {
		m_widgetLayout = survivor->widgetLayout();
        m_cWidget = dynamic_cast<AbstractPaneWidget*>(survivor->widget(0));
        m_cWidget->setParent(this);
        m_cWidget->updateConnections();
//		m_cWidget = new AbstractPaneWidget(this, m_widgetLayout);
//		addWidget(m_cWidget);
		delete survivor;
		m_is_deepest = true;
	} else {
		setOrientation(survivor->orientation());
		setSizes(survivor->sizes());
		AbstractPane* first = static_cast<AbstractPane*>(survivor->widget(0));
		AbstractPane* second = static_cast<AbstractPane*>(survivor->widget(1));
		addWidget(first);
		addWidget(second);
		delete survivor;
	}

//	m_cWidget->setLayout(m_layout);

//	qDebug() << survivor;
	//	addWidget(m_cWidget);
}

bool AbstractPane::deepest()
{
    return m_is_deepest;
}

AbstractPane *AbstractPane::createPane(AbstractPaneLayout* layout, int role)
{
    return new AbstractPane(layout, role);

}



void AbstractPane::switchRole(int role) {
//	switch (role) {
//		case (Gui::XZPaneRole):
//			{
//			delete m_widgetLayout;
//			m_widgetLayout = new XZPaneLayout(m_cWidget);
//			}
//	}
}

int AbstractPane::role()
{
	return m_role;
}

