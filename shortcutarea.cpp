#include "shortcutarea.h"
#include "qdebug.h"
ShortcutArea::ShortcutArea(QWidget* parent) :
	QWidget()
{
	connect(parent,SIGNAL(enterEventTriggered(QEvent *)),this,SLOT(enterEvent(QEvent*)));
	connect(parent,SIGNAL(leaveEventTriggered(QEvent *)),this,SLOT(leaveEvent(QEvent*)));
}

void ShortcutArea::addShortcut(QShortcut *shortcut)
{
	m_shortcuts.insert(shortcut);
	shortcut->setEnabled(false);
}

void ShortcutArea::removeShortcut(QShortcut *shortcut)
{
	m_shortcuts.remove(shortcut);
}

void ShortcutArea::muteAllShortcuts(bool evenEsc)
{
	foreach (QShortcut* s, m_shortcuts) {
		if (evenEsc || s->key() != QKeySequence("Esc")) {
			s->setEnabled(false);
		}
	}
}

void ShortcutArea::unmuteAllShortcuts()
{
	foreach (QShortcut* s, m_shortcuts) {
		s->setEnabled(true);
	}
}


void ShortcutArea::enterEvent(QEvent *) {
	unmuteAllShortcuts();
    qDebug() << "unmuted shortcuts";
}

void ShortcutArea::leaveEvent(QEvent *) {
	muteAllShortcuts(true);
    qDebug() << "muted shortcuts";
}


