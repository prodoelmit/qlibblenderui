#include "abstractblocklayout.h"
#include <QCalendarWidget>
#include "qdebug.h"
AbstractBlockLayout::AbstractBlockLayout(WindowBlock *parentBlock):
    QVBoxLayout(parentBlock), m_parentBlock(parentBlock)
{
	m_menuBar = new QMenuBar(parentBlock);
    m_menuBar->addMenu(parentBlock->menuSwitcher());
	m_workZone = new QSplitter(parentBlock);
	m_labelBar = new QLabel(parentBlock);
	insertWidget(0,m_workZone);
	setStretch(0,0);
	setStretch(0,2);
	insertWidget(1,m_menuBar);
	insertWidget(1,m_labelBar);
	m_labelBar->hide();
}

AbstractBlockLayout::~AbstractBlockLayout()
{

}

QLabel *AbstractBlockLayout::labelBar()
{
    return m_labelBar;
}

WindowBlock *AbstractBlockLayout::parentBlock()
{
	return m_parentBlock;
}

QSplitter *AbstractBlockLayout::workZone()
{
	return m_workZone;
}

void AbstractBlockLayout::showLabelInsteadMenu()
{
//	m_menuBar->hide();
	m_labelBar->show();
}

