#include "windowblock.h"
#include <QCalendarWidget>
#include <QMenuBar>
WindowBlock::WindowBlock(QWidget* parent):
    QWidget(parent)
{
	m_menuSwitcher = new QMenu("Swch",this);
//	QAction* switchToXZAction = new QAction("Plain XZ",this);
//	m_menuSwitcher->addAction(switchToXZAction);
//	connect(switchToXZAction,&QAction::triggered,this,&WindowBlock::switchToXZ);
//	m_menuSwitcher->addAction("Hello two");
	setDefaultLayout();

}

WindowBlock::~WindowBlock()
{

}

QMenu *WindowBlock::menuSwitcher()
{
	return m_menuSwitcher;
}

void WindowBlock::clearLayout()
{
	if (m_layout) delete m_layout;
}

void WindowBlock::setDefaultLayout()
{
    m_layout = new AbstractBlockLayout(this);
	setLayout(m_layout);
}

//void WindowBlock::switchToXZ()
//{
//	clearLayout();
//    m_layout = new XZBlockLayout(this);
//	setLayout(m_layout);
//}




