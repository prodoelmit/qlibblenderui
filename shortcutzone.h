#ifndef SHORTCUTZONE_H
#define SHORTCUTZONE_H
#include <QWidget>
#include <QShortcut>
#include <QList>
class ShortcutZone
{
public:
	ShortcutZone();
	void addShortcut(QShortcut* shortcut);
	void removeShortcut(QShortcut* shortcut);
	void clearShortcuts();
	void enterEvent(QEvent *);
	void leaveEvent(QEvent *);
private:
	QList<QShortcut* > m_shortcuts;
};

#endif // SHORTCUTZONE_H
