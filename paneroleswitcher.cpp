#include "paneroleswitcher.h"

PaneRoleSwitcher::PaneRoleSwitcher(AbstractPane* pane):
	QToolButton(), m_pane(pane)
{
	setText("&switch");
	QMenu* menu = new QMenu();
	setMenu(menu);

}

PaneRoleSwitcher::~PaneRoleSwitcher()
{

}

void PaneRoleSwitcher::setPane(AbstractPane *pane)
{
	m_pane = pane;
}

void PaneRoleSwitcher::switchRole(int role)
{
	m_currentRole = role;
	m_pane->switchRole(role);
}


