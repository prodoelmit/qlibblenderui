#include "abstractpanewidget.h"

AbstractPaneWidget::AbstractPaneWidget(AbstractPane *parent, AbstractPaneLayout* layout) :
	QWidget(parent)
{
	if (layout) {
//		layout->setParent(this);
		setLayout(layout);
	}
	m_splitMenu = new QMenu();
    m_splitHorAction = new QAction("SplitHor",this);
    m_splitVerAction = new QAction("SplitVer",this);
    m_splitMenu->addAction(m_splitHorAction);
    m_splitMenu->addAction(m_splitVerAction);
    m_connectedActions.insert(m_splitHorAction);
    m_connectedActions.insert(m_splitVerAction);
    connect(m_splitHorAction,SIGNAL(triggered()),parentPane(),SLOT(splitHorizontally()));
    connect(m_splitVerAction,SIGNAL(triggered()),parentPane(),SLOT(splitVertically()));
}

AbstractPaneBar *AbstractPaneWidget::bar()
{
	return layout()->bar();
}

AbstractPane *AbstractPaneWidget::parentWidget() const
{
	return qobject_cast<AbstractPane*>(QWidget::parentWidget());
}

AbstractPane *AbstractPaneWidget::parentPane() const
{
	return parentWidget();
}

AbstractPaneLayout *AbstractPaneWidget::layout() const
{
    return qobject_cast<AbstractPaneLayout*>(QWidget::layout());
}

void AbstractPaneWidget::updateConnections()
{
    foreach (QAction* a, m_connectedActions) {
        disconnect(a,0,0,0);
    }
    m_connectedActions.clear();
    connect(m_splitHorAction,SIGNAL(triggered()),parentPane(),SLOT(splitHorizontally()));
    connect(m_splitVerAction,SIGNAL(triggered()),parentPane(),SLOT(splitVertically()));
    m_connectedActions.insert(m_splitHorAction);
    m_connectedActions.insert(m_splitVerAction);
}

void AbstractPaneWidget::mousePressEvent(QMouseEvent *e) {
	if (e->button() == Qt::RightButton) {
		QPoint pos = mapToGlobal(e->pos());
		m_splitMenu->popup(pos);
	}
}
