#ifndef ABSTRACTPANEBAR_H
#define ABSTRACTPANEBAR_H
#include <QStackedWidget>
#include <QToolBar>
#include <QLabel>
#include "abstractpane.h"
#include "abstractpanelayout.h"
#include "qlibblenderui_global.h"
#include "abstractpanewidget.h"

class AbstractPaneWidget;
class AbstractPaneLayout;
class QLIBBLENDERUISHARED_EXPORT AbstractPaneBar: public QStackedWidget
{
	Q_OBJECT
public:
    AbstractPaneBar(AbstractPaneWidget* parent = 0);
	~AbstractPaneBar();
	void startShowingInfo();
	void setInfo(QString info);
	void stopShowingInfo();
    QLabel* label();
private:
	QLabel* m_infoLabel;
	QToolBar* m_toolbar;
	AbstractPaneLayout* m_paneLayout;
};

#endif // ABSTRACTPANEBAR_H
